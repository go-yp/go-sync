# Golang sync examples

### Goroutine cost
##### Test
```go
package benchmarks

import (
	"sync"
	"sync/atomic"
	"testing"
)

func BenchmarkGoroutineCost(b *testing.B) {
	var value uint32
	var wg sync.WaitGroup

	wg.Add(b.N)

	for i := 0; i < b.N; i++ {
		go func() {
			atomic.AddUint32(&value, 1)

			wg.Done()
		}()
	}

	wg.Wait()

	if value != uint32(b.N) {
		b.Errorf("expected %d, got %d", b.N, value)
	}
}
```

##### Result
```text
BenchmarkGoroutineCost   	 3608112	       332 ns/op	       0 B/op	       0 allocs/op
PASS
```

##### Compare
332 ns ~ find max of slice with 256 ints (289 ns)

### Goroutine with channel write and read cost
##### Test
```go
package benchmarks

import "testing"

func BenchmarkGoroutineReturnResultByChannel(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var resultChannel = make(chan bool)

		go func() {
			resultChannel <- true
		}()

		_ = <-resultChannel
		close(resultChannel)
	}
}
```

##### Result
```text
BenchmarkGoroutineReturnResultByChannel   	 1414471	       938 ns/op	      96 B/op	       1 allocs/op
PASS
```
