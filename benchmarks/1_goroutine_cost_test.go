package benchmarks

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

// 332 ns/op	       0 B/op	       0 allocs/op
func BenchmarkGoroutineCost(b *testing.B) {
	var value uint32
	var wg sync.WaitGroup

	wg.Add(b.N)

	for i := 0; i < b.N; i++ {
		go func() {
			atomic.AddUint32(&value, 1)

			wg.Done()
		}()
	}

	wg.Wait()

	if value != uint32(b.N) {
		b.Errorf("expected %d, got %d", b.N, value)
	}
}

type goRuntimeMaxCount struct {
	mu    sync.Mutex
	value int
}

func (c *goRuntimeMaxCount) update() {
	var value = runtime.NumGoroutine()

	c.mu.Lock()
	if value > c.value {
		c.value = value
	}
	c.mu.Unlock()
}

func (c *goRuntimeMaxCount) get() int {
	return c.value
}

func BenchmarkGoroutineCostDump(b *testing.B) {
	var (
		value          = uint32(0)
		wg             = new(sync.WaitGroup)
		goRuntimeCount = new(goRuntimeMaxCount)
	)

	b.Logf("before goroutine count %d", goRuntimeCount.get())

	wg.Add(b.N)

	for i := 0; i < b.N; i++ {
		go func() {
			atomic.AddUint32(&value, 1)

			goRuntimeCount.update()

			wg.Done()
		}()
	}

	wg.Wait()

	if value != uint32(b.N) {
		b.Errorf("expected %d, got %d", b.N, value)
	}

	b.Logf("after goroutine count %d for b.N = %d", goRuntimeCount.get(), b.N)
}

func BenchmarkGoroutineCostOne(b *testing.B) {
	var value uint32
	var wg sync.WaitGroup

	for i := 0; i < b.N; i++ {
		wg.Add(1)
		go func() {
			atomic.AddUint32(&value, 1)

			wg.Done()
		}()
		wg.Wait()
	}

	if value != uint32(b.N) {
		b.Errorf("expected %d, got %d", b.N, value)
	}
}

func BenchmarkGoroutineCostOneOverhead(b *testing.B) {
	var value uint32
	var wg sync.WaitGroup

	for i := 0; i < b.N; i++ {
		wg.Add(1)
		atomic.AddUint32(&value, 1)
		wg.Done()
		wg.Wait()
	}

	if value != uint32(b.N) {
		b.Errorf("expected %d, got %d", b.N, value)
	}
}

// 1 elements		6.25 ns/op
// 2 elements		5.07 ns/op
// 4 elements		6.83 ns/op
// 8 elements		10.7 ns/op
// 16 elements		17.5 ns/op
// 32 elements		44.5 ns/op
// 64 elements		78.1 ns/op
// 128 elements		147 ns/op
// 256 elements		289 ns/op
// 512 elements		585 ns/op
// 1024 elements	1179 ns/op
func BenchmarkGoroutineCostCompare(b *testing.B) {
	if testing.Short() {
		b.Skip("short mode")
	}

	const limit = 1 << 10

	var (
		buffer        = make([]int, limit)
		attemptValues []int
	)

	var (
		max = func(values []int) int {
			length := len(values)

			if length == 0 {
				return 0
			}

			result := values[0]

			for i := 1; i < length; i++ {
				value := values[i]

				if value > result {
					result = value
				}
			}

			return result
		}

		run = func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				_ = max(attemptValues)
			}
		}
	)

	for i := 0; i < limit; i++ {
		buffer[i] = i
	}

	for attempt := 1; attempt <= limit; attempt <<= 1 {
		attemptValues = buffer[:attempt]

		b.Run(fmt.Sprintf("find max of %d elements", attempt), run)
	}
}

func TestParallelPureIncrement(t *testing.T) {
	t.Skip("expect failed")

	const n = 1000000

	var (
		value uint32
		wg    = new(sync.WaitGroup)
	)

	wg.Add(n)
	for i := 0; i < n; i++ {
		go func() {
			value++ // same sa "value = value + 1"

			wg.Done()
		}()
	}

	wg.Wait()

	if value != n {
		t.Errorf("expected %d, got %d", n, value)
	}
}

func TestParallelPureAtomic(t *testing.T) {
	const n = 1000000

	var value uint32

	for i := 0; i < n; i++ {
		go func() {
			atomic.AddUint32(&value, 1)
		}()
	}

	for atomic.LoadUint32(&value) < n {
		runtime.Gosched()
	}
}

func fetchRepositoryStarsByIDs(ids []int) []int {
	var result = make([]int, len(ids))

	for i, value := range ids {
		result[i] = fetchRepositoryStarByID(value)
	}

	return result
}

func fetchRepositoryStarByID(id int) int {
	// emulate slow http request by sleep
	time.Sleep(100 * time.Microsecond)

	// emulate response
	var stars = id % 32

	return stars
}
