package benchmarks

import (
	"sync"
	"testing"
)

func TestGoroutineReturnResultByChannel(t *testing.T) {
	var resultChannel = make(chan bool)

	go func() {
		resultChannel <- true
	}()

	var result = <-resultChannel
	close(resultChannel)

	if result != true {
		t.Errorf("expected true")
	}
}

// 829 ns/op	      96 B/op	       1 allocs/op
func BenchmarkGoroutineReturnResultByChannel(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var resultChannel = make(chan bool)

		go func() {
			resultChannel <- true
		}()

		_ = <-resultChannel
		close(resultChannel)
	}
}

// 2532 ns/op	      96 B/op	       1 allocs/op
func BenchmarkGoroutineReturnResultByChannelTriple(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var resultChannel = make(chan bool)

		go func() {
			resultChannel <- true
		}()
		go func() {
			resultChannel <- true
		}()
		go func() {
			resultChannel <- true
		}()

		_ = <-resultChannel
		_ = <-resultChannel
		_ = <-resultChannel
		close(resultChannel)
	}
}

// 2604 ns/op	     112 B/op	       1 allocs/op
func BenchmarkGoroutineReturnResultByChannelTripleBuffered(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var resultChannel = make(chan bool, 3)

		go func() {
			resultChannel <- true
		}()
		go func() {
			resultChannel <- true
		}()
		go func() {
			resultChannel <- true
		}()

		_ = <-resultChannel
		_ = <-resultChannel
		_ = <-resultChannel
		close(resultChannel)
	}
}

// 1088 ns/op	      64 B/op	       4 allocs/op
func BenchmarkGoroutineReturnResultByMutex(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var (
			wg   sync.WaitGroup
			mu   sync.Mutex
			data []bool
		)

		wg.Add(1)
		go func() {
			mu.Lock()
			data = append(data, true)
			mu.Unlock()

			wg.Done()
		}()

		wg.Wait()
		for range data {
			// NOP
		}
	}
}

// 2324 ns/op	      64 B/op	       4 allocs/op
func BenchmarkGoroutineReturnResultByMutexTriplePreallocate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var (
			wg   sync.WaitGroup
			mu   sync.Mutex
			data = make([]bool, 0, 3)
		)

		// 1
		wg.Add(1)
		go func() {
			mu.Lock()
			data = append(data, true)
			mu.Unlock()

			wg.Done()
		}()

		// 2
		wg.Add(1)
		go func() {
			mu.Lock()
			data = append(data, true)
			mu.Unlock()

			wg.Done()
		}()

		// 3
		wg.Add(1)
		go func() {
			mu.Lock()
			data = append(data, true)
			mu.Unlock()

			wg.Done()
		}()

		wg.Wait()
		for range data {
			// NOP
		}
	}
}
