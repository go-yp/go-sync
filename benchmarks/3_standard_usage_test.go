package benchmarks

import (
	"fmt"
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestStandardUsage(t *testing.T) {
	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests) * time.Millisecond)

		return 1 << requests
	}

	var requests = []uint{1, 2, 3, 4, 5}

	var expect = []uint{2, 4, 8, 16, 32}

	var actual = standardUsage(requests, handler)

	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("expected %+v, got %+v", expect, actual)
	}
}

// run for 1 elements         	  607183	      1669 ns/op	     120 B/op	       3 allocs/op
// run for 2 elements         	  297555	      3962 ns/op	     192 B/op	       4 allocs/op
// run for 4 elements         	  179940	      6331 ns/op	     336 B/op	       6 allocs/op
// run for 8 elements         	   96709	     12022 ns/op	     624 B/op	      10 allocs/op
// run for 16 elements        	   46604	     25011 ns/op	    1200 B/op	      18 allocs/op
// run for 32 elements        	   25275	     48240 ns/op	    2353 B/op	      34 allocs/op
// run for 64 elements        	   13843	     85535 ns/op	    4662 B/op	      66 allocs/op
// run for 128 elements       	    7254	    143162 ns/op	    9284 B/op	     130 allocs/op
// run for 256 elements       	    4640	    250851 ns/op	   18547 B/op	     258 allocs/op
// run for 512 elements       	    2401	    463912 ns/op	   37224 B/op	     517 allocs/op
// run for 1024 elements      	    1202	    941999 ns/op	   75383 B/op	    1042 allocs/op
// run for 2048 elements      	     550	   2117458 ns/op	  155972 B/op	    2136 allocs/op
// run for 4096 elements      	     242	   4758542 ns/op	  318869 B/op	    4343 allocs/op
// run for 8192 elements      	     115	  10477683 ns/op	  648192 B/op	    8799 allocs/op
// run for 16384 elements     	      52	  22223389 ns/op	 1326086 B/op	   17902 allocs/op
// run for 32768 elements     	      25	  46340126 ns/op	 2589682 B/op	   35077 allocs/op
// run for 65536 elements     	      12	 102539870 ns/op	 5780504 B/op	   76383 allocs/op
func BenchmarkStandardUsage(b *testing.B) {
	if testing.Short() {
		b.Skip("short mode")
	}

	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests))

		return 1 << requests
	}

	const limit = 1 << 16
	var buffer = make([]uint, limit)

	var requests []uint

	for i := uint(0); i < limit; i++ {
		buffer[i] = i
	}

	var run = func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = standardUsage(requests, handler)
		}
	}

	for attempt := 1; attempt <= limit; attempt <<= 1 {
		requests = buffer[:attempt]

		b.Run(fmt.Sprintf("run for %d elements", attempt), run)
	}
}

func standardUsage(requests []uint, handler func(uint) uint) []uint {
	var length = len(requests)

	var wg sync.WaitGroup
	var responses = make(chan uint)

	wg.Add(length)

	for _, request := range requests {
		go func(request uint) {
			responses <- handler(request)

			wg.Done()
		}(request)
	}

	go func() {
		wg.Wait()
		close(responses)
	}()

	var result = make([]uint, 0, length)
	for response := range responses {
		result = append(result, response)
	}
	return result
}
