package benchmarks

import (
	"fmt"
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestThrottlingByChannelSend(t *testing.T) {
	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests) * time.Millisecond)

		return 1 << requests
	}

	var requests = []uint{1, 2, 3, 4, 5}

	var expect = []uint{2, 4, 8, 16, 32}

	var actual = usageWithThrottlingByChannelSend(requests, handler, 2)

	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("expected %+v, got %+v", expect, actual)
	}
}

func TestThrottlingByChannelReceive(t *testing.T) {
	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests) * time.Millisecond)

		return 1 << requests
	}

	var requests = []uint{1, 2, 3, 4, 5}

	var expect = []uint{2, 4, 8, 16, 32}

	var actual = usageWithThrottlingByChannelReceive(requests, handler, 2)

	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("expected %+v, got %+v", expect, actual)
	}
}

// throttling by channel send for 1 elements         	  633823	      1878 ns/op	     296 B/op	       4 allocs/op
// throttling by channel receive for 1 elements      	  211976	      5791 ns/op	     296 B/op	       4 allocs/op
// throttling by channel send for 2 elements         	  278073	      5115 ns/op	     368 B/op	       5 allocs/op
// throttling by channel receive for 2 elements      	  147115	      9291 ns/op	     368 B/op	       5 allocs/op
// throttling by channel send for 4 elements         	  200546	      7044 ns/op	     528 B/op	       7 allocs/op
// throttling by channel receive for 4 elements      	  113532	     11044 ns/op	     528 B/op	       7 allocs/op
// throttling by channel send for 8 elements         	   95208	     12375 ns/op	     848 B/op	      11 allocs/op
// throttling by channel receive for 8 elements      	   80254	     14110 ns/op	     848 B/op	      11 allocs/op
// throttling by channel send for 16 elements        	   46342	     27366 ns/op	    1488 B/op	      19 allocs/op
// throttling by channel receive for 16 elements     	   44258	     27752 ns/op	    1488 B/op	      19 allocs/op
// throttling by channel send for 32 elements        	   23775	     49211 ns/op	    2768 B/op	      35 allocs/op
// throttling by channel receive for 32 elements     	   23348	     51908 ns/op	    2768 B/op	      35 allocs/op
// throttling by channel send for 64 elements        	   13612	     90686 ns/op	    5360 B/op	      67 allocs/op
// throttling by channel receive for 64 elements     	   12992	     93038 ns/op	    5360 B/op	      67 allocs/op
// throttling by channel send for 128 elements       	    8162	    134492 ns/op	   10481 B/op	     131 allocs/op
// throttling by channel receive for 128 elements    	    8192	    137327 ns/op	   10480 B/op	     131 allocs/op
// throttling by channel send for 256 elements       	    4569	    245003 ns/op	   20848 B/op	     259 allocs/op
// throttling by channel receive for 256 elements    	    4440	    247167 ns/op	   20849 B/op	     259 allocs/op
// throttling by channel send for 512 elements       	    2240	    512093 ns/op	   41840 B/op	     515 allocs/op
// throttling by channel receive for 512 elements    	    2232	    510946 ns/op	   41842 B/op	     515 allocs/op
// throttling by channel send for 1024 elements      	    1086	   1037836 ns/op	   83313 B/op	    1027 allocs/op
// throttling by channel receive for 1024 elements   	    1105	   1044026 ns/op	   83313 B/op	    1027 allocs/op
// throttling by channel send for 2048 elements      	     554	   2108855 ns/op	  166015 B/op	    2051 allocs/op
// throttling by channel receive for 2048 elements   	     554	   2120884 ns/op	  166002 B/op	    2051 allocs/op
// throttling by channel send for 4096 elements      	     277	   4334455 ns/op	  336001 B/op	    4099 allocs/op
// throttling by channel receive for 4096 elements   	     276	   4318164 ns/op	  336002 B/op	    4099 allocs/op
// throttling by channel send for 8192 elements      	     133	   8886327 ns/op	  663683 B/op	    8195 allocs/op
// throttling by channel receive for 8192 elements   	     133	   8898535 ns/op	  663682 B/op	    8195 allocs/op
// throttling by channel send for 16384 elements     	      58	  19103556 ns/op	 1319118 B/op	   16387 allocs/op
// throttling by channel receive for 16384 elements  	      61	  19152630 ns/op	 1319088 B/op	   16387 allocs/op
// throttling by channel send for 32768 elements     	      25	  43289716 ns/op	 2629770 B/op	   32771 allocs/op
// throttling by channel receive for 32768 elements  	      26	  43445408 ns/op	 2629780 B/op	   32771 allocs/op
// throttling by channel send for 65536 elements     	      10	 106979204 ns/op	 5251580 B/op	   65541 allocs/op
// throttling by channel receive for 65536 elements  	      10	 107867185 ns/op	 5251280 B/op	   65540 allocs/op
func BenchmarkThrottlingByChannel(b *testing.B) {
	if testing.Short() {
		b.Skip("short mode")
	}

	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests))

		return 1 << requests
	}

	const limit = 1 << 16
	const throttling = 64
	var buffer = make([]uint, limit)

	var requests []uint

	for i := uint(0); i < limit; i++ {
		buffer[i] = i
	}

	var runThrottlingByChannelSend = func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = usageWithThrottlingByChannelSend(requests, handler, throttling)
		}
	}

	var runThrottlingByChannelReceive = func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = usageWithThrottlingByChannelReceive(requests, handler, throttling)
		}
	}

	for attempt := 1; attempt <= limit; attempt <<= 1 {
		requests = buffer[:attempt]

		b.Run(fmt.Sprintf("throttling by channel send for %d elements", attempt), runThrottlingByChannelSend)
		b.Run(fmt.Sprintf("throttling by channel receive for %d elements", attempt), runThrottlingByChannelReceive)
	}
}

func usageWithThrottlingByChannelSend(requests []uint, handler func(uint) uint, throttling uint) []uint {
	var length = len(requests)

	var wg sync.WaitGroup
	var responses = make(chan uint, length)
	var throttler = make(chan bool, throttling)

	wg.Add(length)

	for _, request := range requests {
		throttler <- true

		go func(request uint) {
			responses <- handler(request)

			<-throttler
			wg.Done()
		}(request)
	}

	go func() {
		wg.Wait()
		close(responses)
		close(throttler)
	}()

	var result = make([]uint, 0, length)
	for response := range responses {
		result = append(result, response)
	}
	return result
}

func usageWithThrottlingByChannelReceive(requests []uint, handler func(uint) uint, throttling uint) []uint {
	var length = len(requests)

	var wg sync.WaitGroup
	var responseChannel = make(chan uint, length)
	var throttler = make(chan bool, throttling)

	for i := uint(0); i < throttling; i++ {
		throttler <- true
	}

	wg.Add(length)

	for _, request := range requests {
		<-throttler

		go func(request uint) {
			responseChannel <- handler(request)

			throttler <- true
			wg.Done()
		}(request)
	}

	go func() {
		wg.Wait()
		close(responseChannel)
		close(throttler)
	}()

	var result = make([]uint, 0, length)
	for response := range responseChannel {
		result = append(result, response)
	}
	return result
}
