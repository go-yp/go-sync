package benchmarks

import (
	"fmt"
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestThrottlingByWorkers(t *testing.T) {
	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests) * time.Millisecond)

		return 1 << requests
	}

	var requests = []uint{1, 2, 3, 4, 5}

	var expect = []uint{2, 4, 8, 16, 32}

	var actual = usageWithThrottlingByWorkers(requests, handler, 2)

	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("expected %+v, got %+v", expect, actual)
	}
}

// throttling by workers for 1 elements         	   36790	     34436 ns/op	     794 B/op	       5 allocs/op
// throttling by workers for 2 elements         	   39740	     30802 ns/op	     865 B/op	       6 allocs/op
// throttling by workers for 4 elements         	   36777	     33008 ns/op	     938 B/op	       6 allocs/op
// throttling by workers for 8 elements         	   30394	     37856 ns/op	    1074 B/op	       7 allocs/op
// throttling by workers for 16 elements        	   26516	     44358 ns/op	    1303 B/op	       9 allocs/op
// throttling by workers for 32 elements        	   20691	     57009 ns/op	    1917 B/op	      14 allocs/op
// throttling by workers for 64 elements        	   16143	     75793 ns/op	    2925 B/op	      22 allocs/op
// throttling by workers for 128 elements       	    8407	    138132 ns/op	    6894 B/op	      68 allocs/op
// throttling by workers for 256 elements       	    5325	    211539 ns/op	    9147 B/op	      69 allocs/op
// throttling by workers for 512 elements       	    2694	    418151 ns/op	   13772 B/op	      69 allocs/op
// throttling by workers for 1024 elements      	    1304	    868321 ns/op	   22496 B/op	      69 allocs/op
// throttling by workers for 2048 elements      	     668	   1784338 ns/op	   39687 B/op	      70 allocs/op
// throttling by workers for 4096 elements      	     326	   3686164 ns/op	   78667 B/op	      70 allocs/op
// throttling by workers for 8192 elements      	     153	   7783216 ns/op	  144183 B/op	      70 allocs/op
// throttling by workers for 16384 elements     	      67	  17477065 ns/op	  275556 B/op	      73 allocs/op
// throttling by workers for 32768 elements     	      27	  39951870 ns/op	  537411 B/op	      70 allocs/op
// throttling by workers for 65536 elements     	      12	  99934276 ns/op	 1062064 B/op	      74 allocs/op
func BenchmarkThrottlingByWorkers(b *testing.B) {
	if testing.Short() {
		b.Skip("short mode")
	}

	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests))

		return 1 << requests
	}

	const limit = 1 << 16
	const throttling = 64
	var buffer = make([]uint, limit)

	var requests []uint

	for i := uint(0); i < limit; i++ {
		buffer[i] = i
	}

	var run = func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = usageWithThrottlingByWorkers(requests, handler, throttling)
		}
	}

	for attempt := 1; attempt <= limit; attempt <<= 1 {
		requests = buffer[:attempt]

		b.Run(fmt.Sprintf("throttling by workers for %d elements", attempt), run)
	}
}

func usageWithThrottlingByWorkers(requests []uint, handler func(uint) uint, throttling uint) []uint {
	var length = len(requests)

	var wg = new(sync.WaitGroup)
	var requestChannel = make(chan uint, throttling)
	var responseChannel = make(chan uint, length)

	var workerHandler = func(wg *sync.WaitGroup, requestChannel <-chan uint, responseChannel chan<- uint) {
		for request := range requestChannel {
			responseChannel <- handler(request)
		}

		wg.Done()
	}

	for i := uint(0); i < throttling; i++ {
		wg.Add(1)

		go workerHandler(wg, requestChannel, responseChannel)
	}

	for _, request := range requests {
		requestChannel <- request
	}
	close(requestChannel)

	go func() {
		wg.Wait()
		close(responseChannel)
	}()

	var result = make([]uint, 0, length)
	for response := range responseChannel {
		result = append(result, response)
	}
	return result
}
