package benchmarks

import (
	"fmt"
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestThrottlingByWorkersWithGoroutines(t *testing.T) {
	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests) * time.Millisecond)

		return 1 << requests
	}

	var requests = []uint{1, 2, 3, 4, 5}

	var expect = []uint{2, 4, 8, 16, 32}

	var actual = usageWithThrottlingByWorkersWithGouroutines(requests, handler, 2, 2)

	if !reflect.DeepEqual(expect, actual) {
		t.Errorf("expected %+v, got %+v", expect, actual)
	}
}

// throttling by workers with goroutines for 1 elements         	  328688	      3468 ns/op	     432 B/op	      11 allocs/op
// throttling by workers with goroutines for 2 elements         	  218089	      7179 ns/op	     504 B/op	      12 allocs/op
// throttling by workers with goroutines for 4 elements         	  139161	      7314 ns/op	     664 B/op	      14 allocs/op
// throttling by workers with goroutines for 8 elements         	  110347	     11333 ns/op	     984 B/op	      18 allocs/op
// throttling by workers with goroutines for 16 elements        	   51985	     22619 ns/op	    1792 B/op	      28 allocs/op
// throttling by workers with goroutines for 32 elements        	   27582	     43383 ns/op	    3424 B/op	      48 allocs/op
// throttling by workers with goroutines for 64 elements        	   15279	     80239 ns/op	    6689 B/op	      88 allocs/op
// throttling by workers with goroutines for 128 elements       	    8162	    130708 ns/op	   13219 B/op	     168 allocs/op
// throttling by workers with goroutines for 256 elements       	    4507	    251311 ns/op	   26283 B/op	     328 allocs/op
// throttling by workers with goroutines for 512 elements       	    2146	    530892 ns/op	   52394 B/op	     648 allocs/op
// throttling by workers with goroutines for 1024 elements      	    1075	   1104780 ns/op	  104622 B/op	    1288 allocs/op
// throttling by workers with goroutines for 2048 elements      	     522	   2266908 ns/op	  209075 B/op	    2568 allocs/op
// throttling by workers with goroutines for 4096 elements      	     254	   4629036 ns/op	  417956 B/op	    5128 allocs/op
// throttling by workers with goroutines for 8192 elements      	     123	   9625264 ns/op	  835753 B/op	   10248 allocs/op
// throttling by workers with goroutines for 16384 elements     	      55	  20500555 ns/op	 1671404 B/op	   20488 allocs/op
// throttling by workers with goroutines for 32768 elements     	      22	  46020867 ns/op	 3342583 B/op	   40968 allocs/op
// throttling by workers with goroutines for 65536 elements     	       9	 116159541 ns/op	 6684885 B/op	   81928 allocs/op
func BenchmarkThrottlingByWorkersWithGoroutines(b *testing.B) {
	if testing.Short() {
		b.Skip("short mode")
	}

	var handler = func(requests uint) uint {
		time.Sleep(time.Duration(requests))

		return 1 << requests
	}

	const limit = 1 << 16
	const workerCount = 8
	const packSize = 8
	var buffer = make([]uint, limit)

	var requests []uint

	for i := uint(0); i < limit; i++ {
		buffer[i] = i
	}

	var run = func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = usageWithThrottlingByWorkersWithGouroutines(requests, handler, workerCount, packSize)
		}
	}

	for attempt := 1; attempt <= limit; attempt <<= 1 {
		requests = buffer[:attempt]

		b.Run(fmt.Sprintf("throttling by workers with goroutines for %d elements", attempt), run)
	}
}

func usageWithThrottlingByWorkersWithGouroutines(requests []uint, handler func(uint) uint, workerCount, packSize int) []uint {
	var length = len(requests)

	var rangeResultList = RangesBySize(length, packSize)
	var rangeCount = len(rangeResultList)

	var wg = new(sync.WaitGroup)
	var requestPackChannel = make(chan []uint, rangeCount)
	var responsePackChannel = make(chan chan uint, rangeCount)

	var workerHandler = func(wg *sync.WaitGroup, requestPackChannel <-chan []uint, responsePackChannel chan<- chan uint) {
		for requestPack := range requestPackChannel {
			var length = len(requestPack)
			var wg sync.WaitGroup
			var currentResponsePack = make(chan uint, length)

			wg.Add(length)
			for _, request := range requestPack {
				go func(request uint) {
					currentResponsePack <- handler(request)

					wg.Done()
				}(request)
			}

			wg.Wait()
			responsePackChannel <- currentResponsePack
			close(currentResponsePack)
		}

		wg.Done()
	}

	if workerCount > rangeCount {
		workerCount = rangeCount
	}

	wg.Add(workerCount)
	for i := 0; i < workerCount; i++ {
		go workerHandler(wg, requestPackChannel, responsePackChannel)
	}

	for _, rangeResult := range RangesBySize(length, packSize) {
		requestPackChannel <- requests[rangeResult.Begin:rangeResult.End]
	}
	close(requestPackChannel)

	go func() {
		wg.Wait()
		close(responsePackChannel)
	}()

	var result = make([]uint, 0, length)
	for responsePack := range responsePackChannel {
		for response := range responsePack {
			result = append(result, response)
		}
	}
	return result
}
