package benchmarks

type RangeResult struct {
	Begin int
	End   int
}

func RangesBySize(originLength, packLength int) []RangeResult {
	if originLength <= packLength {
		return []RangeResult{
			{
				Begin: 0,
				End:   originLength,
			},
		}
	}

	var length = originLength / packLength
	var part = originLength%packLength > 0
	var result []RangeResult

	if part {
		result = make([]RangeResult, 0, length+1)
	} else {
		result = make([]RangeResult, 0, length)
	}

	var begin = 0
	var end = packLength

	for i := 0; i < length; i++ {
		result = append(result, RangeResult{
			Begin: begin,
			End:   end,
		})

		begin, end = end, end+packLength
	}

	if part {
		result = append(result, RangeResult{begin, originLength})
	}

	return result
}
