module gitlab.com/go-yp/go-sync

go 1.13

require (
	github.com/gopereza/packer v0.0.0-20190831230559-f993410d479a
	github.com/stretchr/testify v1.4.0 // indirect
)
